import { add } from '../add'

describe('add', () => {
    test.each`
        a    | b    | expected
        ${1} | ${2} | ${3}
        ${1} | ${3} | ${4}
        ${2} | ${3} | ${5}
    `('should return expected result $expected = $a + $b', ({ a, b, expected }) => {
        const actual = add(a, b)

        expect(expected).toBe(actual)
    })
})