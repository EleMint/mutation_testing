package cli

import (
	"fmt"
	"os"

	"gitlab.com/EleMint/Mutation_Testing/mutator"
	"gitlab.com/EleMint/Mutation_Testing/mutator/cli/commands"
)

func Start() {
	initCmd := commands.NewInitCommand()
	runCmd := commands.NewRunCommand()

	if len(os.Args) == 1 {
		usage()
	}

	switch os.Args[1] {
	case "init":
		initCmd.Parse(os.Args[2:])
	case "run":
		runCmd.Parse(os.Args[2:])
	case "version":
		version()
	case "help":
		version()
		usage()
	default:
		fmt.Printf("No command matching '%s'\n\n", os.Args[1])
		usage()
	}
}

func usage() {
	fmt.Println("\nUsage: mut <command> [<args>]")
	fmt.Println("\nCommands:")
	fmt.Println("\tinit   \t\tCreates mut default config file")
	fmt.Println("\trun    \t\tRuns mut with config file")
	fmt.Println("\tversion\t\tPrints mut version")
	fmt.Println("\thelp   \t\tPrints this message and exits with error 1")
	os.Exit(1)
}

func version() {
	fmt.Printf("mut::%s\n", mutator.VERSION)
}
