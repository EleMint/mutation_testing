package commands

import (
	"flag"
	"os"

	"gitlab.com/EleMint/Mutation_Testing/mutator/cli/configuration"
)

type RunCommand struct {
	cmd  *flag.FlagSet
	help *bool
	show *bool

	config *configuration.Config
}

func NewRunCommand() *RunCommand {
	cmd := flag.NewFlagSet("run", flag.ExitOnError)

	help := cmd.Bool("help", false, "Prints this message and exits with error 1")
	show := cmd.Bool("show-config", false, "Prints the current running configuration and exits with error 1")

	return &RunCommand{
		cmd:  cmd,
		help: help,
		show: show,

		config: &configuration.Config{},
	}
}

func (command *RunCommand) Parse(args []string) {
	command.cmd.Parse(args)

	if !command.cmd.Parsed() || *command.help {
		command.cmd.PrintDefaults()
		os.Exit(1)
	}

	command.Start()
}

func (command *RunCommand) Start() {
	command.config.Parse(command.show)
}
