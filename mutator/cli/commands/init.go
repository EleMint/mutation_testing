package commands

import (
	"flag"
	"os"
	"strings"

	"gitlab.com/EleMint/Mutation_Testing/mutator/cli/configuration"
)

type InitCommand struct {
	cmd  *flag.FlagSet
	help *bool

	config *configuration.Config
}

func NewInitCommand() *InitCommand {
	cmd := flag.NewFlagSet("init", flag.ExitOnError)
	help := cmd.Bool("help", false, "Prints this message and exits with error 1")

	runner := cmd.String("runner", "npm test", "Command to use to test")
	base := cmd.String("base", "./src", "Base path to project files")
	sandbox := cmd.String("sandbox", ".mut_sandbox", "Mutator sandbox dir")
	suffix := cmd.String("suffix", ".test.*", "Test file suffix")
	exclude := cmd.String("exclude", "dist", "Relative paths not copied to sandbox env")

	exclude_split := strings.Split(*exclude, ",")

	return &InitCommand{
		cmd:  cmd,
		help: help,

		config: &configuration.Config{
			Runner:  runner,
			Base:    base,
			Sandbox: sandbox,
			Suffix:  suffix,
			Exclude: &exclude_split,
		},
	}
}

func (command *InitCommand) Parse(args []string) {
	command.cmd.Parse(args)

	if !command.cmd.Parsed() || *command.help {
		command.cmd.PrintDefaults()
		os.Exit(1)
	}

	command.Start()
}

func (command *InitCommand) Start() {
	command.config.Write()
}
