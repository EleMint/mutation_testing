package configuration

import (
	"fmt"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v3"
)

const CONFIG_PATH = "./.mut.config.yml"

type Config struct {
	Runner  *string
	Base    *string
	Sandbox *string
	Suffix  *string
	Exclude *[]string
}

func (c *Config) Write() {
	yfile, err := yaml.Marshal(c)
	if err != nil {
		fmt.Printf("error parsing configuration: %v\n", err)
		os.Exit(1)
	}

	err = ioutil.WriteFile(CONFIG_PATH, yfile, 0666)
	if err != nil {
		fmt.Printf("error writing configuration to file: %v\n", err)
		os.Exit(1)
	}

	fmt.Printf("configuration written: '%s'\n", CONFIG_PATH)
}

func (c *Config) Parse(show *bool) {
	yfile, err := ioutil.ReadFile(CONFIG_PATH)
	if err != nil {
		fmt.Printf("error reading configuration: %v\n", err)
		os.Exit(1)
	}

	err = yaml.Unmarshal(yfile, c)
	if err != nil {
		fmt.Printf("error parsing configuration: %v\n", err)
		os.Exit(1)
	}

	if *show {
		c.Print()
	}
}

func (c *Config) Print() {
	fmt.Printf("Config: %s\n\n", CONFIG_PATH)
	fmt.Printf("Runner: '%v'\n", *c.Runner)
	fmt.Printf("Base: '%v'\n", *c.Base)
	fmt.Printf("Sandbox: '%v'\n", *c.Sandbox)
	fmt.Printf("Suffix: '%v'\n", *c.Suffix)
	fmt.Printf("Exclude: %v\n", *c.Exclude)

	os.Exit(1)
}
